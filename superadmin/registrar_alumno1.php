<!DOCTYPE html>
<html lang="en">
<head>
<title>Bootstrap Example</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/sizing.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
<div style="margin-top: 100px;"></div>
<div class="container">
	<div class="col-md-10 col-md-offset-1">
		<div class="panel panel-primary">
			<div class="panel-heading">Registrar Perfil</div>
			<div class="panel-body">
				<div class="col-md-12">
				<!-- 	<h4 class="text-primary">Datos Personales</h4> -->
					<div class="form-group">
					  <label for="sel1">Tipo de Perfil:</label>
					  <select class="form-control" id="sel1">
					    <option>Selecciona el perfil</option>
					    <option>Alumno</option>
					    <option>Moderador</option>
					    <option>Profesor</option>
					  </select>
					</div>
				</div>
				<form class="" action="" method="POST">

					<div class="col-md-12">
						<div class="form-group">
							<!-- <label for="email">Nombres:</label> -->
							<input type="text" class="form-control" id="first_name" placeholder="Nombres">
						</div>
						<div class="form-group">
							<!-- <label for="pwd">Apellidos:</label> -->
							<input type="text" class="form-control" id="last_name" placeholder="Apellidos">
						</div>
						<div class="form-group">
							<!-- <label for="pwd">Email:</label> -->
							<input type="email" class="form-control" id="email" placeholder="Email">
						</div>
						<div class="form-group">
							<!-- <label for="pwd">Teléfono:</label> -->
							<input type="tel" class="form-control" id="phone" placeholder="Teléfono">
						</div>
						<div class="form-group">
							<!-- <label for="pwd">Dirección:</label> -->
							<input type="text" class="form-control" id="address" placeholder="Dirección">
						</div>
						<div class="form-group">
							<!-- <label for="pwd">Dirección:</label> -->
							<input type="password" class="form-control" id="password" placeholder="Contraseña">
						</div>

					</div>
					<div class="col-md-12 text-right">
						<button class="btn btn-success" id="btn_save">Registrar</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>
</div>

</body>