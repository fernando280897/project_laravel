<!DOCTYPE html>
<html lang="es">
<head>
<title>Solicitar Clase</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
  <link rel="stylesheet" href="css/font-awesome.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
  <div class="container p-5">
      <div class="card">
          <div class="card-header">Solicitar Clase</div>
          <div class="card-block">
            <fieldset class="">
              <!-- <legend class="scheduler-border">Solicita tu clase</legend> -->
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="sel1">Curso:</label>
                    <select class="form-control" id="sel1">
                      <option>Matemática</option>
                      <option>Lenguaje</option>
                      <option>Ingles</option>
                      <option>Biologia</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="sel1">Nivel:</label>
                    <select class="form-control" id="sel1">
                      <option>1° Secundaria</option>
                      <option>2° Secundaria</option>
                      <option>3° Secundaria</option>
                      <option>4° Secundaria</option>
                      <option>5° Secundaria</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="sel1"></label>
                    <input type="text" class="form-control" id="" placeholder="Centro Educativo">
                  </div>
                </div>
              </div>
              <div class="row">
                <!-- 2 row -->
                <div class="col-md-4">
                  <div class="form-group">
                    <div class='input-group date' id='datetimepicker1'>
                      <input type='text' class="form-control" placeholder="Fecha/Hora" />
                      <span class="input-group-addon">
                        <span class="fa fa-calendar"></span>
                      </span>
                    </div>
                  </div>
                  <div class="form-group">
                    <button type="button" class="btn btn-primary btn-md " data-toggle="modal" data-target="#asignar_alumno">Asignar Alumno</button>  
                  </div>
                  <div class="form-group">
                     <input type="text" class="form-control" disabled="disabled" value="Fernando Juarez Rodriguez">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="sel1">Adjuntar Tarea:</label>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <!-- <label for="comment">Comment:</label> -->
                    <textarea class="form-control" rows="5" id="comment" placeholder="Personaliza tu clase(Como podemos ayudarte)"></textarea>
                  </div>
                </div>
                <div class="col-md-4">

                </div>
                <div class="col-md-8 text-right">
                  <button type="button" class="btn btn-primary btn-md">Pedir Clase</button>
                </div>
              </div>
            </fieldset>
          </div>
      </div>
    </div>


  <!-- Modal asignar profesor -->

  <div class="modal fade" id="asignar_alumno" tabindex="-1" role="dialog" aria-labelledby="asignar_prof" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="exampleModalLabel">Asignar Alumno</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>  
        <div class="col-md-12 p-3">
          <form>
            <div class="input-group form-group">
              <input type="text" class="form-control" placeholder="Ingresar nombre del alumno que desea asignar">
              <div class="input-group-btn">
                <button class="btn btn-default" type="submit">
                  <i class="fa fa-search" ></i>
                </button>
              </div>
            </div>
          </form>
        </div>
        <div class="col-md-12">
          <div class="card">
            <div class="card-block">
              <div class="row">
                <table class="table table-hover">
                  <thead class="thead-inverse">
                    <tr>
                      <th>Nombres y Apellidos</th>
                      <th>Curso / Nivel</th>
                      <th>Asignar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>John</td>
                      <td>Ingles I</td>
                      <td>
                        <div class="">
                          <label><input type="radio" name="optradio"></label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>Mary</td>
                      <td>Lenguaje</td>
                      <td>
                        <div class="">
                          <label><input type="radio" name="optradio"></label>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>July</td>
                      <td>Matemática</td>
                      <td>
                        <div class="">
                          <label><input type="radio" name="optradio"></label>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 text-right p-3">
          <div class="form-group">
            <button class="btn btn-primary">Asignar</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- End modal -->


  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/tether.min.js"></script>
  <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
  <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
  <script type="text/javascript">
    $(function () {
      $('#datetimepicker1').datetimepicker();
    });
  </script>
</body>
</html>