<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container vertical-container">
  	<div class="col-md-8">
  		<div class="panel panel-primary">
		  <div class="panel-heading">Login</div>
			  <div class="panel-body">
			  	<div class="row">
			  		<div class="col-md-8 col-md-offset-2">
			  			<div class="text-center">
					  		<img src="images/male32.png" class="img-circle img-responsive center-block" alt="User">
					  	</div>
					  	<br>
					  	<form >
						  <div class="input-group">
						    
						    <input id="email" type="text" class="form-control" name="email" placeholder="Email">
						    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						  </div>
						  <br>
						  <div class="input-group">
						    
						    <input id="password" type="password" class="form-control" name="password" placeholder="Password">
						    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						  </div>
						  <br>
						  <div class="row">
						  	<div class="col-md-12">
						  		<div class="text-center">
								  	<a href="listado_clases.php" type="button" class="btn btn-primary btn-md">Ingresar</a>
								  </div>
						  	</div>

						  	<div class="text-center">
						  		<br>
						  		<br>
						  		<a href="#">¿Has olvidado tu contraseña?</a>
						  	</div>

						  </div>
						</form>
			  		</div>
			  	</div>
			  </div>
		</div>
  	</div>
</div>

</body>
</html>