  <!DOCTYPE html>
  <html lang="es">
  <head>
  <title>Listado Clase</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>

   <!-- Listados clases -->
    
    <br>
    <br>
   <div class="container">
      <div class="card">
        <div class="card-header">Detalle de Pago</div>
        <div class="panel-body">
 <!--          <fieldset class="col-md-12">    -->  
            <!-- <legend >Listado Clases</legend> -->
            <div class="card-block">
                <table class="table table-hover">
                  <thead class="thead-default">
                    <tr class="text-center">
                      <th>Curso / Nivel</th>
                      <th>Centro Educativo</th>
                      <th>Fecha - Hora</th>
                      <th>Alumno</th>
                      <th>Lugar</th>
                      <th>Estado</th>
                      <th>Precio</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Matemática</td>
                      <td>Sophianum</td>
                      <td>01/10/17</td>
                      <td>Fernando Juarez</td>
                      <td>
                        <div class="col-md-10">
                          <input class="form-control" id="ex1" type="text" disabled="disabled">
                        </div>    
                      </td>
                      <td>Espera</td>
                      <td>
                        <div class="col-md-10">
                          <input class="form-control" id="ex1" type="text" disabled="disabled">
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>Ingles</td>
                      <td>UPC</td>
                      <td>29/09/17</td>
                      <td>Luis Manuel</td>
                     <td>
                      <div class="col-md-10">
                        <input class="form-control" id="ex1" type="text" disabled="disabled">
                      </div>    
                    </td>
                    <td>Espera</td>
                    <td>
                      <div class="col-md-10">
                        <input class="form-control" id="ex1" type="text" disabled="disabled">
                      </div>    
                    </td>
                  </tr>
                  <tr>
                    <td>Matemática</td>
                    <td>Sophianum</td>
                    <td>26/09/17</td>
                    <td>Ricardo Lopez</td>
                   <td>
                    <div class="col-md-10">
                      <input class="form-control" id="ex1" type="text" disabled="disabled">
                    </div>    
                  </td>
                  <td class="success">Atendido</td>
                  <td>
                    <div class="col-md-10">
                      <input class="form-control" id="ex1" type="text" value="S./ 50" disabled="disabled">
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </fieldset>
        </div>
      </div>
  </div>



<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>