<!DOCTYPE html>
<html lang="es">
<head>
	<title>Listado Clase</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/font-awesome.css">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div class="container">
		<br/>
		<div class="panel panel-default">
			
			<div class="panel-heading">Fieldset Exemple</div>
			<div class="panel-body">
				
				<fieldset class="col-md-6">    	
					<legend>Fieldset Title</legend>
					
					<div class="panel panel-default">
						<div class="panel-body">
							<p>Fieldset content...</p>
						</div>
					</div>
					
				</fieldset>				
				
				<div class="clearfix"></div>
			</div>
			
		</div>
	</div>

<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>