<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <div class="container">
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Solicita tu clase</legend>
          <div class="form-group row">
            <div class="col-md-4">
                <label for="sel1">Curso:</label>
                <select class="form-control" id="sel1">
                  <option>Matemática</option>
                  <option>Lenguaje</option>
                  <option>Ingles</option>
                  <option>Biologia</option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="sel1">Nivel:</label>
                <select class="form-control" id="sel1">
                  <option>1° Secundaria</option>
                  <option>2° Secundaria</option>
                  <option>3° Secundaria</option>
                  <option>4° Secundaria</option>
                  <option>5° Secundaria</option>
                </select>
            </div>
            <div class="col-md-4">
                <label for="sel1"></label>
                <input type="text" class="form-control" id="" placeholder="Centro Educativo">
              </div>
          </div>
          <div class="form-group row">
            <!-- 2 row -->
            <div class="col-md-4">
                <div class='input-group date' id='datetimepicker1'>
                  <input type='text' class="form-control" placeholder="Fecha/Hora" />
                  <span class="input-group-addon">
                  <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
            </div>
            <div class="col-md-4">
              <label for="sel1">Adjuntar Tarea:</label>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                  <!-- <label for="comment">Comment:</label> -->
                  <textarea class="form-control" rows="5" id="comment" placeholder="Personaliza tu clase(Como podemos ayudarte)"></textarea>
                </div>
            </div>
            <div class="col-md-12 text-right">
              <div class="form-group">
                <button type="button" class="btn btn-primary btn-md">Pedir</button>
              </div>
            </div>
          </div>
        </fieldset>
      </div>

      <!-- Mis pedidos -->

      <div class="container">
        <fieldset class="scheduler-border">
          <legend class="scheduler-border">Mis Pedidos</legend>
          <div class="row">
            <div class="col-md-12">
            	<table class="table table-hover">
				    <thead>
				      <tr>
				        <th>Curso / Nivel</th>
				        <th>Centro Educativo</th>
				        <th>Fecha - Hora</th>
				        <th>Adjunto</th>
				        <th>Profesor Asignado</th>
				        <th>Lugar</th>
				        <th>Estado</th>
				        <th>Precio</th>
				        <th>Pagado</th>
				        <th>Encuesta</th>
				      </tr>
				    </thead>
				    <tbody>
				      <tr>
				        <td>Matemática</td>
				        <td>Sophianum</td>
				        <td>01/10/17</td>
				        <td></td>
				        <td>Jirafales</td>
				        <td>Domicilio</td>
				        <td>Espera</td>
				        <td></td>
				        <td></td>
				        <td></td>
				      </tr>
				      <tr>
				        <td>Ingles</td>
				        <td>UPC</td>
				        <td>29/09/17</td>
				        <td></td>
				        <td>Adolfo</td>
				        <td>Domicilio</td>
				        <td>Espera</td>
				        <td></td>
				        <td></td>
				        <td></td>
				      </tr>
				      <tr>
				        <td>Matemática</td>
				        <td>Sophianum</td>
				        <td>26/09/17</td>
				        <td class="text-center">
				        	<a href="#"><span class="glyphicon glyphicon-folder-open"></span></a>
				        </td>
				        <td>Jirafales</td>
				        <td>Domicilio</td>
				        <td class="bg-success">Atendido</td>
				        <td>S/.20</td>
				        <td class="text-center">
				        	<a href="#"><span class="fa fa-check"></span></a>
				        </td>
				        <td class="text-center">
				        	<a href="#"><span class="fa fa-list-alt"></span></a>
				        </td>
				      </tr>
				    </tbody>
				  </table>
            </div>
          </div>
          <div class="row">
            <!-- 2 row -->
            
          </div>
        </fieldset>
      </div>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/tether.min.js"></script>
    <script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
    <script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
    <script type="text/javascript">
      $(function () {
          $('#datetimepicker1').datetimepicker();
      });
    </script>
  </body>
</html>