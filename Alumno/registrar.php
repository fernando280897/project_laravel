<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div style="margin-top: 100px;"></div>
<div class="container">
	<div class="col-md-10 col-md-offset-1">
		<div class="card">
			<div class="card-header">Registrar Perfil</div>
			<div class="card-block">
			  	<form class="" action="" method="POST">

				    <div class="form-group row">
				    	<div class="col-md-6">
				    		<div class="form-group">
				    			<input type="text" class="form-control" id="email" placeholder="Nombres">
				    		</div>
				    		<div class="form-group">
							    <!-- <label for="pwd">Apellidos:</label> -->
							    <input type="text" class="form-control" id="last_name" placeholder="Apellidos">
							</div>
							<div class="form-group">
							    <!-- <label for="pwd">Email:</label> -->
							    <input type="email" class="form-control" id="email" placeholder="Email">
							  </div>
							  <div class="form-group">
							    <!-- <label for="pwd">Teléfono:</label> -->
							    <input type="tel" class="form-control" id="phone" placeholder="Teléfono">
							  </div>
							  <div class="form-group">
							    <!-- <label for="pwd">Dirección:</label> -->
							    <input type="text" class="form-control" id="address" placeholder="Dirección">
							  </div>
							   <div class="form-group">
							    <!-- <label for="pwd">Dirección:</label> -->
							    <input type="password" class="form-control" id="password" placeholder="Contraseña">
							  </div>
							  <div class="form-group">
							    <!-- <label for="pwd">Dirección:</label> -->
							    <input type="text" class="form-control" id="r_password" placeholder="Repetir Contraseña">
							  </div>
				    	</div>
				    	<div class="col-md-6">
				    		<div class="form-group">
				    			<textarea class="form-control" rows="8" style="max-width: 100%;" id="comment" placeholder="Otros Datos de Interes"></textarea>
				    		</div>
				    		<div class="form-group">
				    			<div class="text-right">
									<a href="#" type="submit" class="btn btn-warning" id="bt_save">Registrar</a >
								</div>
				    		</div>
				    	</div>

				   	</div>
				</form>
  			</div>
		</div>
	</div>
</div>
</body>
</html>