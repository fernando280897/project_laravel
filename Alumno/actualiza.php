<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container vertical-container">
  	<div class="col-md-8">
  		<ul class="nav nav-tabs">
		  <li class="active"><a data-toggle="tab" href="#home">Perfil</a></li>
		  <li><a data-toggle="tab" href="#menu1">Actualiza Contraseña</a></li>
		</ul>

		<div class="tab-content">
			<div id="home" class="tab-pane fade in active">
				<div class="col-md-2 text-center" style="margin:20px 0 25px 0;">
			    	<span class="btn btn-success btn-file">
					    Subir<input type="file">
					</span>

					<p class="text-center" style="margin:20px 0 25px 0;">Actualizar Foto de Perfil</p>
			    </div>
			    <br>
				<form class="" action="" method="POST">

				    <div class="col-md-7">
							  <div class="form-group">
							    <!-- <label for="email">Nombres:</label> -->
							    <input type="text" class="form-control" id="email" placeholder="Nombres">
							  </div>
							  <div class="form-group">
							    <!-- <label for="pwd">Apellidos:</label> -->
							    <input type="text" class="form-control" id="pwd" placeholder="Apellidos">
							  </div>
							  <div class="form-group">
							    <!-- <label for="pwd">Email:</label> -->
							    <input type="email" class="form-control" id="pwd" placeholder="Email">
							  </div>
							  <div class="form-group">
							    <!-- <label for="pwd">Teléfono:</label> -->
							    <input type="tel" class="form-control" id="pwd" placeholder="Teléfono">
							  </div>
							  <div class="form-group">
							    <!-- <label for="pwd">Dirección:</label> -->
							    <input type="text" class="form-control" id="pwd" placeholder="Dirección">
							  </div>

					    </div>
					    <div class="col-md-3">
					    	<div class="form-group">
							  <textarea class="form-control" rows="8" style="max-width: 100%;" id="comment" placeholder="Otros Datos de Interes"></textarea>
							</div>
							<a href="solicita_clase.php" type="submit" class="btn btn-default">Actualiza Datos</a>
					    </div>
					  	
				</form>

			    <!-- end tab 1 -->
			</div>


		<div id="menu1" class="tab-pane fade">
			<br>
			<div class="col-md-12">
				<form class="" action="" method="POST">
						  <div class="form-group">
						    <input type="password" class="form-control" id="email" placeholder="Actual">
						  </div>
						  <div class="form-group">
						    <input type="password" class="form-control" id="pwd" placeholder="Nueva Contraseña">
						  </div>
						  <div class="form-group">
						    <input type="password" class="form-control" id="pwd" placeholder="Repetir Contraseña">
						  </div>
						<div class="text-left">
					  		<a href="#">¿Has olvidado tu contraseña?</a>
					  	</div>

				  	<div class="text-right">
				  		<a href="solicita_clase.php" type="submit" class="btn btn-default">Actualiza Datos</a>
				  	</div>

				</form>
			</div>
		</div>
	</div>
</div>

</body>
</html>