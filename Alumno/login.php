<!DOCTYPE html>
<html lang="en">
<head>
  <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container vertical-container">
  	<div class="col-md-8">
  		<div class="panel panel-primary">
		  <div class="panel-heading">Login</div>
			  <div class="panel-body">
			  	<div class="row">
			  		<div class="col-md-8 col-md-offset-2">
			  			<div class="text-center">
					  		<img src="images/user.png" class="img-circle" alt="User">
					  	</div>
					  	<br>
					  	<form >
						  <div class="input-group">
						    
						    <input id="email" type="text" class="form-control" name="email" placeholder="Email">
						    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						  </div>
						  <br>
						  <div class="input-group">
						    
						    <input id="password" type="password" class="form-control" name="password" placeholder="Password">
						    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
						  </div>
						  <br>
						  <div class="row">
						  	<div class="col-md-6">
						  		<div class="text-right">
								  	<a href="actualiza.php" type="button" class="btn btn-primary btn-md">Ingresar</a>
								  </div>
						  	</div>
						  	<div class="col-md-6">
						  		<div class="text-left">
								  	 <a href="" type="button" class="btn btn-primary btn-md">
									    <span class="glyphicon glyphicon-log-in"></span> Facebook
									  </a>
								  </div>
						  	</div>
							<br>
							<br>
						  	<div class="text-center">
						  		<a href="#">¿Has olvidado tu contraseña?</a>
						  	</div>

						  	<div class="col-md-12">
						  		<br>
						  		<div class="text-center">
								  	<a href="registrar.php" type="button" class="btn btn-primary btn-md" id="btn_save">Registrate</a>
								  </div>
						  	</div>

						  </div>
						</form>
			  		</div>
			  	</div>
			  </div>
		</div>
  	</div>
</div>

</body>
</html>