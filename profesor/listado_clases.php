  <!DOCTYPE html>
  <html lang="es">
  <head>
  <title>Listado Clase</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/sizing.css">
  </head>
  <body>

   <!-- Listados clases -->
    
    <br>
    <br>
   <div class="container">
      <div class="panel panel-primary">
        <div class="panel-heading">Listado de Clases</div>
        <div class="panel-body">
 <!--          <fieldset class="col-md-12">    -->  
            <!-- <legend >Listado Clases</legend> -->
            <div class="table-responsive row">
                <table class="table table-hover">
                  <thead>
                    <tr class="text-center">
                      <th>Curso / Nivel</th>
                      <th>Centro Educativo</th>
                      <th>Fecha - Hora</th>
                      <th>Adjunto</th>
                      <th>Alumno</th>
                      <th>Lugar</th>
                      <th>Estado</th>
                      <th>Observaciones</th>
                      <!-- <th>Precio</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Matemática</td>
                      <td>Sophianum</td>
                      <td>01/10/17 - 10:20 AM</td>
                      <td class="text-center">
                        <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                      </td>
                      <td>Fernando Juarez</td>
                      <td>
                        <div class="">
                          <input class="form-control w-100" id="ex1" type="text">
                        </div>    
                      </td>
                      <td>Espera</td>
                      <td class="text-center">
                        <a href="#" type="button" data-toggle="modal" data-target="#observaciones"><i class="glyphicon glyphicon-comment"></i></a>
                      </td>
                    </tr>
                    <tr>
                      <td>Ingles</td>
                      <td>UPC</td>
                      <td>29/09/17</td>
                      <td class="text-center">
                        <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                      </td>
                      <td>Luis Manuel</td>
                     <td>
                      <div class="">
                        <input class="form-control w-100 " id="ex1" type="text">
                      </div>    
                    </td>
                    <td>Espera</td>
                    <td class="text-center">
                      <a href="#" type="button" data-toggle="modal" data-target="#observaciones"><i class="glyphicon glyphicon-comment"></i></a>
                    </td>
                  </tr>
                  <tr>
                    <td>Matemática</td>
                    <td>Sophianum</td>
                    <td>26/09/17</td>
                    <td class="text-center">
                      <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                    </td>
                    <td>Ricardo Lopez</td>
                   <td>
                    <div class="">
                      <input class="form-control w-100 " id="ex1" type="text">
                    </div>    
                  </td>
                  <td class="success">Atendido</td>
                  <td class="text-center">
                    <a href="#" type="button" data-toggle="modal" data-target="#observaciones"><i class="glyphicon glyphicon-comment"></i></a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
  <!--       </fieldset> -->
        </div>
      </div>
  </div>

<!-- Modal observaciones -->


  <div class="modal fade" id="observaciones" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Observaciones</h4>
        </div>
        <div class="modal-body">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos ullam sed aperiam tempore laudantium.</p>
        </div>
         <div class="modal-footer">
            <span>Fernando Juarez Rodriguez</span>
        </div>
      </div>
    </div>
  </div>


<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>
</html>