  <!DOCTYPE html>
  <html lang="es">
  <head>
  <title>Listado Clase</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://v4-alpha.getbootstrap.com/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>

   <!-- Listados clases -->

   <div class="container p-5">
      <div class="card">
        <div class="card-header">Detalle del pedido</div>
        <div class="card-block">
 <!--          <fieldset class="col-md-12">    -->  
            <!-- <legend >Listado Clases</legend> -->
            <div class="table-responsive">
                <table class="table table-hover">
                  <thead>
                    <tr class="text-center">
                      <th>Curso / Nivel</th>
                      <th>Centro Educativo</th>
                      <th>Fecha - Hora</th>
                      <th>Adjunto</th>
                      <th>Alumno</th>
                      <th>Lugar</th>
                      <th>Estado</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Matemática</td>
                      <td>Sophianum</td>
                      <td>01/10/17 - 10:20 AM</td>
                      <td class="text-center">
                        <a href="#"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                      </td>
                      <td>Fernando Juarez</td>
                      <td>
                        <div class="">
                          <input class="form-control w-100" id="ex1" type="text" disabled="disabled">
                        </div>    
                      </td>
                      <td>
                        <div class="">
                          <input class="form-control w-75" id="ex1" type="text" placeholder="Estado">
                        </div>
                      </td>
                    </tr>
                
              </tbody>
            </table>
          </div>
  <!--       </fieldset> -->
        </div>
      </div>

      <!-- End  -->


      <div class="card">
        <div class="card-header">Mi perfil</div>
          <fieldset>
            <div class="card-block">
            <div class="row middle-xs">
              <div class="col-md-10 text-center">
                  <div class="box">
                      <h2 class="text-primary"><strong>Prof. Jirafales</strong></h2>
                  </div>
              </div>
              <div class="col-md-2 text-center">
                  <div class="box">
                       <img src="images/male.png" alt="" class="img-responsive center-block" style="width: 50%;">
                      <div style="padding-top: 20px;padding-bottom: 20px;">
                        <a href="#" class="btn btn-primary">Subir Contenido</a>
                      </div>
                  </div>
              </div>
          </div>
          <div class="row">
            <div class="col-md-8 col-md-offset-1">


              <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#profile" role="tab">Perfil</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#experiencia">Experiencia</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#a_compartidos">Archivos Compartidos</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" data-toggle="tab" href="#observaciones">Observaciones</a>
                </li>
              </ul>

              <div class="tab-content">
                <div id="profile" class="tab-pane active" role="tabpanel">
                  <div class="form-group p-3">
                    <textarea class="form-control w-100" rows="10" id="comment" placeholder=""></textarea>
                  </div>
                </div>
                <div id="experiencia" class="tab-pane" role="tabpanel">
                  <div class="form-group p-3">
                      <textarea class="form-control w-100" rows="10" id="comment" placeholder="Experiencia"></textarea>
                  </div>
                </div>
                <div id="a_compartidos" class="tab-pane" role="tabpanel">
                  <br>
                  <div class="col-md-12">
                      <span class="text-danger">No hay archivos!!!!</span>
                  </div>
                </div>
                <div id="observaciones" class="tab-pane" role="tabpanel">
                  <br>
                  <div class="col-md-12">
                      <span class="text-danger">No hay Observaciones!!!!</span>
                  </div>
                </div>
              </div>
           </div>

          </div>
          </div>
        </div>
          </fieldset>
      </div>



  </div>

<!-- Modal observaciones -->


  <div class="modal fade" id="observaciones" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Observaciones</h4>
        </div>
        <div class="modal-body">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos ullam sed aperiam tempore laudantium.</p>
        </div>
         <div class="modal-footer">
            <span>Fernando Juarez Rodriguez</span>
        </div>
      </div>
    </div>
  </div>


<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://v4-alpha.getbootstrap.com/assets/js/vendor/tether.min.js"></script>
<script src="https://v4-alpha.getbootstrap.com/dist/js/bootstrap.min.js"></script>
</body>
</html>